# ipaft
last edited 18/06/2018 by Aw Jia Yu
___

Index Prediction Analysis for Trading (testing phase: focus on 3 markets USA/HK/SG + banks i.e. DBS)

The purpose of IPAFT is to undertake the following:

1. cleaning/ labelling of features using [philipperemy dataset](https://github.com/philipperemy/financial-news-dataset)
<br><br>
- using NLTK package to create a lexicon to parse market information according to the following categories
    - financial instruments: equity, treasuries, oil, gold, FX, macroeconomic factors
    - sentiment: strong negative, negative, neutral, positive, strong positive 
<br><br>
- Proxification of sentiment based on index movements the following day
    - S&P500, NASDAQ, DJIA, GLD, OIL, VIX, STI, HSI
<br><br>     
- Next Innovation
