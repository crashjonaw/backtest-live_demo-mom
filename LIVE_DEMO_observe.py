# -*- coding: utf-8 -*-
"""
Created on Thu Feb 22 13:08:17 2018

Momentum Strategy (W-L)

Details:
    Lakonishok/ Jeegadesh ideology for promoting a strategy with zero cost
    using winners minus losers.
n-k-m:
    - J: observe the past J months for stocks for high returns
    - N: wait for N number of months before the portfolio is initialised
    - K: hold the portfolio for K months
    
@author: Aw Jia Yu
"""
import pandas as pd
import numpy as np
from dateutil.relativedelta import relativedelta
from datetime import date

#Parameters
#J=6  #basis of the stocks' returns in the past J months to be observed
#K=12 #period to hold the portfolio
#N=0  #interval between formation period and holding period.
#percent_mkt_cap=0.3
#percent_W_L = 0.05
#percent_EM=0.5
#day = date(2016,1,1)
def observe(J,K,N,percent_W_L,day,percent_mkt_cap,EM='OFF'):
    back_date = day - relativedelta(months=J)
    end_date = day + relativedelta(months=N+K)-relativedelta(days=1)
    init_date = day + relativedelta(months=N)
    life_span = (end_date - init_date).days
    df= pd.read_csv('sgx_stocks_review_v2.csv',index_col=0)
    df['DELIST_date'] = pd.to_datetime(df['DELIST_date'],errors='coerce')
    df['START_date'] = pd.to_datetime(df['START_date'],errors='ignore')
    avail_stocks = list(df['name'][(df['DELIST_date']>=end_date)&(df['START_date']<=back_date)])
    print('No. of stocks to chew: %s'%len(avail_stocks))
    
    #Removing companies with market cap < _%
    mkt_cap = []
    for stock in avail_stocks:
        try:
            df = pd.read_csv('sgx_stocks_f\%s'%stock.replace('Time Series Data.CSV','Company Accounts Data.CSV'), index_col=0)
        except:
            df = pd.read_csv('sgx_stocks_f\%s'%stock.replace('Time Series Data1.CSV','Company Accounts Data1.CSV'), index_col=0)
        if str(day.year-1) in list(df):
            mkt_cap.append(float(list(df[str(day.year-1)][df['TYPE']=='MV   '])[0]))
        else:
            mkt_cap.append(0)

    df = pd.DataFrame.from_dict({'avail_stocks':avail_stocks,'mkt_cap':mkt_cap})
    df = df.fillna(0)
    df = df.sort_values('mkt_cap')
    df2=df[df['mkt_cap']!=0]
    filtered_stocks = list(df2['avail_stocks'])
    filtered_stocks = filtered_stocks[int(percent_mkt_cap*(len(filtered_stocks))):]
    print('No. of filtered stocks to chew: %s'%len(filtered_stocks))

    if EM == 'OFF':
        stock_list =  []
        data = {'stocks':[], 'mean_returns_%s_months'%J:[]}
        counter = 0
        max_count = len(filtered_stocks)
        for stock in filtered_stocks:
            df = pd.read_csv('sgx_stocks_clean\%s'%stock, index_col=0, parse_dates=True)
            returns = df['Return'][(df.index >= np.datetime64(back_date)) &
                        (df.index<= np.datetime64(day))]
            if returns.mean()==returns.mean() and returns.mean()!=np.inf: 
                stock_list.append((stock,returns.mean()))
                counter+=1
                data['stocks'].append(stock)
                data['mean_returns_%s_months'%J].append(returns.mean())
                #print('Done with %s!\n Progress: %s out of %s stocks!'
                    #  %(stock,counter,max_count))
            else:
                max_count -= 1
                print('stock %s does not exist within the date period stated'%stock)
        sl = sorted(stock_list, key=lambda i: i[1])
        
        winners = [w[0] for w in sl[-1:-int(percent_W_L*len(sl))-1:-1]]
        winners_weights =  [1/len(winners) for w in range(len(winners))]
        losers = [l[0] for l in sl[:int(percent_W_L*len(sl))]]
        losers_weights = [1/len(losers) for l in range(len(losers))]
        
        end_date = end_date+relativedelta(days=1)
        obs = {'W':winners, 'WW':winners_weights, 'L':losers, 'LW':losers_weights, 'init_date':init_date,'LS':life_span,'end_date':end_date}
        
        data_df = pd.DataFrame.from_dict(data)
        data_df = data_df[['stocks','mean_returns_%s_months'%J]]
        return obs
        
    #Accounting for fundamentals - Enterprise Multiple
    else:
        EM_list = []
        for stock in filtered_stocks:
            try:
                df = pd.read_csv('sgx_stocks_f\%s'%stock.replace('Time Series Data.CSV','Company Accounts Data.CSV'), index_col=0)
            except:
                df = pd.read_csv('sgx_stocks_f\%s'%stock.replace('Time Series Data1.CSV','Company Accounts Data1.CSV'), index_col=0)
            if str(day.year) in list(df):
                EM_val = float(list(df[str(day.year)][df['TYPE']=='1504'])[0])/float(list(df[str(day.year)][df['TYPE']=='1502'])[0]) #1502 = EBITDA, 1504 = EV
                if EM_val > 0:    
                    EM_list.append(EM_val)
                else: 
                    EM_list.append(0)
            else:
                EM_list.append(0)
                
        df = pd.DataFrame.from_dict({'avail_stocks':filtered_stocks,'EM_list':EM_list})
        df = df.fillna(np.infty)
        df = df.sort_values('EM_list')
        df2=df[df['EM_list']!=0]
        filtered_stocks = list(df2['avail_stocks'])
        EM_high = filtered_stocks[-int(EM*(len(filtered_stocks))):] #overvalued - glamour
        EM_low = filtered_stocks[:int(EM*(len(filtered_stocks)))] #undervalued - value
        print('No. of EM_low stocks to chew after EM: %s'%len(EM_low)) 
        print('No. of EM_high stocks to chew after EM: %s'%len(EM_high)) 
        job = 'EM_low'
        for _ in [EM_low, EM_high]:
            filtered_stocks = _
            stock_list =  []
            data = {'stocks':[], 'mean_returns_%s_months_%s'%(J,job):[]}
            counter = 0
            max_count = len(filtered_stocks)
            for stock in filtered_stocks:
                df = pd.read_csv('sgx_stocks_clean\%s'%stock, index_col=0, parse_dates=True)
                returns = df['Return'][(df.index >= np.datetime64(back_date)) &
                            (df.index<= np.datetime64(day))]
                if returns.mean()==returns.mean() and returns.mean()!=np.inf: 
                    stock_list.append((stock,returns.mean()))
                    counter+=1
                    data['stocks'].append(stock)
                    data['mean_returns_%s_months_%s'%(J,job)].append(returns.mean())
                    #print('Done with %s!\n Progress: %s out of %s stocks!'
                        #  %(stock,counter,max_count))
                else:
                    max_count -= 1
                    print('stock %s does not exist within the date period stated'%stock)
            sl = sorted(stock_list, key=lambda i: i[1])
            if job == 'EM_low':
                winners = [w[0] for w in sl[-1:-int(percent_W_L*len(sl))-1:-1]]
                winners_weights =  [1/len(winners) for w in range(len(winners))]
                #losers = [l[0] for l in sl[:int(percent_W_L*len(sl))]]
                #losers_weights = [1/len(losers) for l in range(len(losers))]
                print('Done with job %s'%job)
                #data_df = pd.DataFrame.from_dict(data)
                #data_df = data_df[['stocks','mean_returns_%s_months_%s'%(J,job)]]
                #data_df.to_csv('filter\%s_%s_summary.csv'%(job,day.strftime('%d-%m-%Y')),index=None)
                job = 'EM_high'
            elif job == 'EM_high':
                losers = [l[0] for l in sl[:int(percent_W_L*len(sl))]]
                losers_weights = [1/len(losers) for l in range(len(losers))]
                print('Done with job %s'%job)
                #data_df = pd.DataFrame.from_dict(data)
                #data_df = data_df[['stocks','mean_returns_%s_months_%s'%(J,job)]]
                #data_df.to_csv('filter\%s_%s_summary.csv'%(job,day.strftime('%d-%m-%Y')),index=None)
                job = None
            else:
                pass  
            
        end_date = end_date+relativedelta(days=1)
        obs = {'W':winners, 'WW':winners_weights, 'L':losers, 'LW':losers_weights, 'init_date':init_date,'LS':life_span,'end_date':end_date}   
        return obs
'''
if __name__ == '__main__':
    observe(J,K,N,percent_W_L,day,percent_mkt_cap,EM=percent_EM)
'''






