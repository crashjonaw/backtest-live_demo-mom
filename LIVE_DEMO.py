# -*- coding: utf-8 -*-
"""
Created on Tue Jan 30 16:35:54 2018

Strategy:
    1. define the strategy involved
    2. implement strategy
    
output:
    1.performance of each component of the strategy: individual stock returns
    2.volatility of the returns (moving volatility)
    3.regress returns of the strategy using the Cartner 4-factor model
    4.return excel list of returns based on the strategy
    5.obtain changing weights based on the strategy

@author: Aw Jia Yu
"""
import pandas as pd
import numpy as np
import os, shutil
import statsmodels.formula.api as smf
import random
from datetime import timedelta, date
from dateutil.relativedelta import relativedelta
import pickle

'''
parameters:
    1. stocks in portfolio at any given time t = t
    2. weights of individual stocks in portfolio at anytime t = t 
    3. any form of leverage?
    4. adjustments along the timeline?
    
requirements:
    dataframe - column of returns for each stock in the time horizon specified
    dictionary - columns for the weight of each stock at any given time
    dictionary - amount of $ allocated for each stock at any given time
    
'''
#strategy 1: WML 6-0-3
class portfolio():
    def __init__(self, stocks, names, weights, life, d,position):
        self.data = dict(zip(stocks,weights))
        self.position = position #long/short
        self.stock_names = names
        self.name = d.strftime('%Y-%m-%d') + position
        self.life = life
        self.born = d.strftime('%Y-%m-%d')
        self.n=0 #boolean to figure first date
        datelist = pd.date_range(d, periods=life)
        self.datelist = datelist
        self.df = pd.DataFrame(np.nan,index=datelist, columns=
                               [stock+'_return' for stock in stocks]+
                               stocks+
                               [stock+'_start' for stock in stocks]+
                               [stock+'_end' for stock in stocks]+
                               ['P_weight'])
        for stock in stocks:
            df = pd.read_csv('sgx_stocks_clean\%s'%stock, index_col =0, parse_dates=True)
            for day in datelist:
                if day not in df.index.tolist():
                    continue
                else:
                    self.df.at[day,stock+'_return'] = df.at[day,'Return']
            self.df[stock+'_return'].fillna(0,inplace=True)
            self.df.at[d,stock] = self.data[stock]
            self.df.at[d,stock+'_start'] = self.data[stock]
            #print('Done collecting info for stock %s!'%stock)
        print('Now simulating "%s" portfolio! with life %s days!'%(self.position, self.life))
        
    def rebalance(self,w,d):
        for stock in self.data.keys():
            self.df.at[d, stock] =  w[stock]     
            
    def compare_index(self, ix_name):
        df = pd.read_csv('index\\'+'%s.csv'%(ix_name), index_col='Date')
        df.index = pd.to_datetime(df.index)
        self.df['%s_return'%(ix_name)] = np.nan 
        self.df['%s_port'%(ix_name)] = np.nan     
        for day in self.datelist:
                #print('processing..', str(day))
                if day not in df.index.tolist():
                    continue
                else:
                    self.df.at[day,'%s_return'%ix_name] = df.at[day,'%s_return'%ix_name]         
        self.df['%s_return'%ix_name].fillna(0,inplace=True)
        self.df['%s_return'%ix_name.split('^')[1]]=self.df['%s_return'%ix_name]
        self.df['%s_port'%ix_name] = (1 + self.df['%s_return'%ix_name]).cumprod()
    
    def portfolio_analysis(self):
        #file_list = os.listdir('index')
        #index_list = [_.split('.')[0] for _ in file_list]
        #self.compare_index_list = ['^sti_d','^spx_d','^hsi_d']
        self.compare_index_list = ['^sti_d']
        for index in self.compare_index_list:
            self.compare_index(index)
            print(index, ' compared!')
        self.df['P_return']=self.df['P_weight'].pct_change()
        for i in [15,30,50]:
            self.df['%s_rolling_std_R'%i] = np.round(self.df['P_return'].rolling(window=i).std(),5)
        #self.reg_model = smf.ols('P_return ~ sti_d_return', data = self.df).fit()
        #print(self.reg_model.summary())
    
    def plot(self,name):
        import matplotlib.pyplot as plt
        from matplotlib import style
        style.use('ggplot')
        (self.df['P_weight']-1).plot()
        for index in self.compare_index_list:
            (self.df['%s_port'%index]-1).plot()
        plt.legend(loc= 2)
        plt.xlabel('Date')
        plt.ylabel('Cumulative Return')
        plt.savefig(name + '\portfolios'+ '\\%s_%s.png'%(self.born,self.name),dpi=1000,bbox_inches='tight')
        plt.show()
                
        
class strategy():
    def __init__(self, name):
        self.name = name
        self.pfs = []
        self.pfs_end=[]
        if not os.path.exists(name):
            os.makedirs(name)
        if not os.path.exists(name + '\portfolios'):
            os.makedirs(name + '\portfolios') 
        
    def create_portfolio(self,stocks,stock_names, weights,t,start_date,end_date, position):
        if end_date != None:
            t = (end_date - start_date).days
        else:
            pass
        new_p = portfolio(stocks,stock_names,weights,t,start_date,position)
        self.pfs.append(new_p)
        self.d = start_date
        #store_path = self.name + '\portfolios'+ '\\%s.csv'%(str(d))
        #new_p.df.to_csv(store_path)   
        
    def plot_last(self):
        import matplotlib.pyplot as plt
        from matplotlib import style
        style.use('ggplot')        
        #file_list = os.listdir('index')
        #index_list = [_.split('.')[0] for _ in file_list]
        #compare_index_list = ['^sti_d','^spx_d','^hsi_d']
        compare_index_list = ['^sti_d']
        self.output_df.index=pd.to_datetime(self.output_df.index)
        for index in compare_index_list:
            print('starting index %s'%index)
            df = pd.read_csv('index\\'+'%s.csv'%(index), index_col='Date')
            df.index = pd.to_datetime(df.index)
            self.output_df['%s_return'%(index)] = np.nan 
            self.output_df['%s_port'%(index)] = np.nan     
            for day in self.output_df.index:
                    print('processing %s...'%day)
                    #print('processing..', day)
                    if day not in df.index.tolist():
                        continue
                    else:
                        self.output_df.at[day,'%s_return'%index] = df.at[day,'%s_return'%index]         
            self.output_df['%s_return'%index].fillna(0,inplace=True)
            self.output_df['%s_port'%index] =    (1 + self.output_df['%s_return'%index]).cumprod()
            print('Done comparing with index %s!'%index)
        for index in compare_index_list:
            (self.output_df['%s_port'%index]-1).plot()
        self.output_df['total_weight'].plot()  
        #for pf in self.pfs_end:
         #   pf.df['P_weight'].plot()
        plt.legend(loc= 2, prop={'size': 6})
        plt.xlabel('Date')
        plt.ylabel('Total Cumulative Return')
        plt.savefig('%s\overall-graph.png'%self.name,dpi=1000,bbox_inches='tight')
        #plt.savefig('%s\\overall-graph.png'%(self.name),dpi=1000,bbox_inches='tight')
        plt.show()
        
    def run_strategy(self,start_date,end_date,mom, formation_interval):
        daterange = pd.date_range(start_date, end_date)
        self.daterange = daterange
        formation_list = []
        _ = start_date
        while _ < end_date:
            formation_list.append(pd.to_datetime(_.strftime('%Y-%m-%d')))
            _ += formation_interval
        #formation_list = formation_list[:-mom['H']]
        print(formation_list)
        year = None
        #month = start_date.month
        for day in daterange:
            if day.year != year:
                print('Begin Year %s'%(day.year))
            else:
                pass
            year = day.year
            #insert portfolio here!
            if pd.to_datetime(day, yearfirst=True) in formation_list:
                print('Making positions on %s.'%day)
                obs = observe(mom['O'],mom['H'],mom['W'], mom['%'],day, mom['%m'],mom['%EM']) #observe past 6 months hold 3 months
                print('No. of winners: %s \nNo. of losers: %s'%(len(obs['W']),len(obs['L'])))
                print('winners: \n ', obs['W'])
                print('losers: \n ', obs['L'])
                if len(obs['W'])!=0:
                    test.create_portfolio(obs['W'],obs['W'],obs['WW'],obs['LS'],
                                          obs['init_date'],obs['end_date'],'long')
                else:
                    pass

                if len(obs['L'])!=0 :
                    test.create_portfolio(obs['L'],obs['L'],obs['LW'],obs['LS'],
                                          obs['init_date'],obs['end_date'],'short')
                else:
                    pass
                print('No. of portfolios created: %s'%len(self.pfs))
                
            if len(self.pfs) == 0:
                continue
            else:               
                for pf in self.pfs:
                    total = 0
                    for stock in pf.data.keys():
                        prev_day = day - timedelta(1)
                        if pf.n==0:
                            pass
                        else:
                            #insert any rebalancing of weights here!
                            pf.df.at[day,stock] = pf.df.at[prev_day,stock]
                            #beg_value = pf.df.at[prev_day,'P_weight']*pf.df.at[day,stock]
                            beg_value = pf.df.at[prev_day,stock+'_end']
                            pf.df.at[day,stock+'_start'] = beg_value
                            '''
                        if abs(pf.df.at[day,stock+'_return'])>2 : #limiter
                            end_value=pf.df.at[day,stock+'_start']
                        else:
                            '''
                        end_value = (1+pf.df.at[day,stock+'_return'])*pf.df.at[day,stock+'_start']
                        pf.df.at[day,stock+'_end'] = end_value
                        total +=end_value            
                    pf.df.at[day,'P_weight']= total
                    pf.n+=1 
                    
                for pf in self.pfs:
                    pf.life -=1
                    if pf.life == 0:
                        print('Portfolio with %s position died from lifespan on %s!'%(pf.position, day))
                        pf.portfolio_analysis()
                        store_path = self.name + '\portfolios'+ '\\%s_%s.csv'%(pf.born,pf.name)
                        pf.df.to_csv(store_path)
                        pf.plot(self.name)
                        self.pfs_end.append(pf)
                        with open(self.name + '\portfolios'+ '\\%s_%s.pickle'%(pf.born,pf.name),'wb') as f:
                            pickle.dump(pf, f)                           
                    else:
                        pass  
                    
                dummy = [pf for pf in self.pfs if pf.life>0]
                self.pfs = dummy
                #prevent error in iteration
        if len(self.pfs) == 0:
            pass
        else:
            for pf in self.pfs:
                print('Portfolio with %s position died!'%(pf.position))
                pf.portfolio_analysis()
                store_path = self.name + '\portfolios'+ '\\%s_%s.csv'%(pf.born,pf.name)
                pf.df.to_csv(store_path)
                pf.plot(self.name)
                self.pfs_end.append(pf)
                with open(self.name + '\portfolios'+ '\\%s_%s.pickle'%(pf.born,pf.name),'wb') as f:
                    pickle.dump(pf, f)
               #self.pfs.remove(pf)

        self.output_df = pd.DataFrame(np.nan,index=daterange, columns=
                               [pf.name+'_return' for pf in self.pfs_end]+
                               [pf.name+'_weight' for pf in self.pfs_end]+
                               ['total_return','total_weight'])
        self.output_df.index=pd.to_datetime(self.output_df.index)
        #print(self.pfs_end)
        pf_count = 0
        for pf in self.pfs_end:
            for day in daterange:
                #print('processing..', str(day))
                if day not in pf.df.index.tolist():
                    #print('hi')
                    continue
                else:
                    if pf.position == 'long':
                        self.output_df.at[day,pf.name+'_return'] = pf.df.at[day,'P_return']
                        self.output_df.at[day,pf.name+'_weight'] = pf.df.at[day,'P_weight']-1 
                    if pf.position == 'short':
                        self.output_df.at[day,pf.name+'_return'] = -pf.df.at[day,'P_return']
                        self.output_df.at[day,pf.name+'_weight'] = 1-pf.df.at[day,'P_weight'] 
                #print('done with day %s'%day)
            self.output_df[pf.name+'_return'].fillna(0,inplace=True)
            self.output_df[pf.name+'_weight'].fillna(method='ffill',inplace=True)
            pf_count +=1
            print('portfolio completed: %s out of %s!'%(pf_count,len(self.pfs_end)))
        self.output_df['total_return'] = self.output_df[[pf.name+'_return' for pf in self.pfs_end]].sum(axis=1)
        print('done total return!')
        #print(self.output_df['total_return'] )
        self.output_df['total_weight'] = self.output_df[[pf.name+'_weight' for pf in self.pfs_end]].sum(axis=1)
        print('done total weight!')
        #print(self.output_df['total_weight'])
        store_path = self.name + '\\%s.csv'%(self.name)
        self.plot_last()
        self.output_df.to_csv(store_path) 
        with open(self.name + '\\%s.pickle'%(self.name),'wb') as f:
            pickle.dump(self, f)
        print('strategy is completed!')
            
        
                   
if __name__ == '__main__':
    from LIVE_DEMO_observe import observe
    import datetime
    #import subprocess
    #import timeit
    #start = timeit.timeit()
    print('Initialising Momentum Strategy J-0-K')
    start_strat_date = datetime.datetime.strptime(input('start date of strategy?'),'%b %d %Y')
    end_strat_date = datetime.datetime.strptime(input('end date of strategy?'),'%b %d %Y')
    J = int(input("Value of J: ?"))
    N = 0 
    K = int(input("Value of K: ?"))
    i = int(input("Formation Interval (in months): ?"))
    F = float(input("Filter Coefficient (market value distribution): ?"))
    T = float(input("Long/Short Coefficent: ?"))
    #start_strat_date = date(2017,1, 1)
    #end_strat_date = date(2017,12,31)
    formation_interval = relativedelta(years=0,months=i,days=0)
    historical_observation = J #months
    wait = N #months
    hold = K #months
    percent_threshold = T # %filter top and bottom 1%
    percent_mkt_cap = F # %mkt cap
    percent_EM='OFF'
    mom = {'O':historical_observation, 'W':wait, 'H':hold, '%':percent_threshold, '%m': percent_mkt_cap, '%EM':percent_EM}
    strategy_name = 'momentum_%s-%s-%s_interval=%s_WL_percent=%s'%(historical_observation,wait,hold,formation_interval,percent_threshold)
    print('running %s'%strategy_name)
    test = strategy(strategy_name)
    test.run_strategy(start_strat_date,end_strat_date,mom, formation_interval)
    '''    
    #replotting of final graphs
    pickle_in = open('MOM_2008-2017_6-0-12_interval=relativedelta(years=+1)_WL_percent=0.1_2008-01-01_2017-12-30.pickle','rb')
    pf = pickle.load(pickle_in)
    print('pickle loaded!')
    pf.plot_last()

    #subprocess.call(["shutdown", "/s"])
    #end = timeit.timeit()
    #print('Time taken = %s'%(end-start))
    '''
    '''
    for ps in range(2):
        if ps == 0:
            position = 'long'
        else:
            position = 'short'
        stocks = random.sample(sgx_list,n)
        stock_names = [sgx['Company Name'][sgx['Symbol']==x] for x in stocks]
        print (stock_names)
        #weights = np.random.dirichlet(np.ones(len(stocks)),size=1)[0]
        weights = [1/len(stocks) for stock in stocks]
        life_span = None
        '''



    '''
    random selection
    
    for n in range(11)[5:]:
        for i in range(20)[1:]:
            sgx = pd.read_csv('sg_stock_ticker_list.csv')  
            start_strat_date = date(2009, 1, 1)
            sgx_list = sgx['Symbol'][sgx['Fetch']!='Fail'][sgx['IPO_year']<=str(start_strat_date.year)].tolist()
            stocks = random.sample(sgx_list,n)
            stock_names = [sgx['Company Name'][sgx['Symbol']==x] for x in stocks]
            print (stock_names)
            #weights = np.random.dirichlet(np.ones(len(stocks)),size=1)[0]
            weights = [1/len(stocks) for stock in stocks]
            life_span = None
            test.create_portfolio(stocks,stock_names,weights,life_span,start_strat_date, date(2017,12,31),'long')
    '''

        
'''
pickle_in = open('2010-01-01_P8A.SI-S45U.SI-5MD.SI-H30.SI-B73.SI.pickle','rb')
pf = pickle.load(pickle_in)
pf.reg_model.summary()
pf.df
'''


















